// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONGOING303_Ongoing303GameMode_generated_h
#error "Ongoing303GameMode.generated.h already included, missing '#pragma once' in Ongoing303GameMode.h"
#endif
#define ONGOING303_Ongoing303GameMode_generated_h

#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_SPARSE_DATA
#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_RPC_WRAPPERS
#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOngoing303GameMode(); \
	friend struct Z_Construct_UClass_AOngoing303GameMode_Statics; \
public: \
	DECLARE_CLASS(AOngoing303GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ongoing303"), ONGOING303_API) \
	DECLARE_SERIALIZER(AOngoing303GameMode)


#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOngoing303GameMode(); \
	friend struct Z_Construct_UClass_AOngoing303GameMode_Statics; \
public: \
	DECLARE_CLASS(AOngoing303GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ongoing303"), ONGOING303_API) \
	DECLARE_SERIALIZER(AOngoing303GameMode)


#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ONGOING303_API AOngoing303GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOngoing303GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONGOING303_API, AOngoing303GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOngoing303GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONGOING303_API AOngoing303GameMode(AOngoing303GameMode&&); \
	ONGOING303_API AOngoing303GameMode(const AOngoing303GameMode&); \
public:


#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ONGOING303_API AOngoing303GameMode(AOngoing303GameMode&&); \
	ONGOING303_API AOngoing303GameMode(const AOngoing303GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ONGOING303_API, AOngoing303GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOngoing303GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOngoing303GameMode)


#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_9_PROLOG
#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_SPARSE_DATA \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_RPC_WRAPPERS \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_INCLASS \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_SPARSE_DATA \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_INCLASS_NO_PURE_DECLS \
	Ongoing303_Source_Ongoing303_Ongoing303GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONGOING303_API UClass* StaticClass<class AOngoing303GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ongoing303_Source_Ongoing303_Ongoing303GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
