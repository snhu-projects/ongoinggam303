// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONGOING303_Ongoing303Character_generated_h
#error "Ongoing303Character.generated.h already included, missing '#pragma once' in Ongoing303Character.h"
#endif
#define ONGOING303_Ongoing303Character_generated_h

#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_SPARSE_DATA
#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_RPC_WRAPPERS
#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOngoing303Character(); \
	friend struct Z_Construct_UClass_AOngoing303Character_Statics; \
public: \
	DECLARE_CLASS(AOngoing303Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ongoing303"), NO_API) \
	DECLARE_SERIALIZER(AOngoing303Character)


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAOngoing303Character(); \
	friend struct Z_Construct_UClass_AOngoing303Character_Statics; \
public: \
	DECLARE_CLASS(AOngoing303Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Ongoing303"), NO_API) \
	DECLARE_SERIALIZER(AOngoing303Character)


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOngoing303Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOngoing303Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOngoing303Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOngoing303Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOngoing303Character(AOngoing303Character&&); \
	NO_API AOngoing303Character(const AOngoing303Character&); \
public:


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOngoing303Character(AOngoing303Character&&); \
	NO_API AOngoing303Character(const AOngoing303Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOngoing303Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOngoing303Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOngoing303Character)


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AOngoing303Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AOngoing303Character, FollowCamera); }


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_9_PROLOG
#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_PRIVATE_PROPERTY_OFFSET \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_SPARSE_DATA \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_RPC_WRAPPERS \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_INCLASS \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_PRIVATE_PROPERTY_OFFSET \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_SPARSE_DATA \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_INCLASS_NO_PURE_DECLS \
	Ongoing303_Source_Ongoing303_Ongoing303Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONGOING303_API UClass* StaticClass<class AOngoing303Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ongoing303_Source_Ongoing303_Ongoing303Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
