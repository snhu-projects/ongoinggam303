// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Ongoing303/Ongoing303GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOngoing303GameMode() {}
// Cross Module References
	ONGOING303_API UClass* Z_Construct_UClass_AOngoing303GameMode_NoRegister();
	ONGOING303_API UClass* Z_Construct_UClass_AOngoing303GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Ongoing303();
// End Cross Module References
	void AOngoing303GameMode::StaticRegisterNativesAOngoing303GameMode()
	{
	}
	UClass* Z_Construct_UClass_AOngoing303GameMode_NoRegister()
	{
		return AOngoing303GameMode::StaticClass();
	}
	struct Z_Construct_UClass_AOngoing303GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOngoing303GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Ongoing303,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOngoing303GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Ongoing303GameMode.h" },
		{ "ModuleRelativePath", "Ongoing303GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOngoing303GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOngoing303GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOngoing303GameMode_Statics::ClassParams = {
		&AOngoing303GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AOngoing303GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AOngoing303GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOngoing303GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOngoing303GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOngoing303GameMode, 3041467784);
	template<> ONGOING303_API UClass* StaticClass<AOngoing303GameMode>()
	{
		return AOngoing303GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOngoing303GameMode(Z_Construct_UClass_AOngoing303GameMode, &AOngoing303GameMode::StaticClass, TEXT("/Script/Ongoing303"), TEXT("AOngoing303GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOngoing303GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
