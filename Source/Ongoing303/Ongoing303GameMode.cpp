// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Ongoing303GameMode.h"
#include "Ongoing303Character.h"
#include "UObject/ConstructorHelpers.h"

AOngoing303GameMode::AOngoing303GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
