// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ongoing303GameMode.generated.h"

UCLASS(minimalapi)
class AOngoing303GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOngoing303GameMode();
};



